<?php
/**
 * @class VMUtc
 * Description of VMUtc class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMUtc extends CApplicationComponent
{
    const MYSQL_DATETIME = 'Y-m-d H:i:s';

    /**
     * Get user local datetime offset from UTC in seconds
     *
     * @return integer $offset
     * @throws CException
     */
    public function currentDateTimeOffset()
    {
        $currentDatetimeOffset = Yii::app()->session->itemAt('currentDateTimeOffset');
        if (!$currentDatetimeOffset) {
	        return 0; // return UTC
        }

        return Yii::app()->session->itemAt('currentDateTimeOffset');
    }

    /**
     * Return string with datetime converted from user locale to UTC
     *
     * @param DateTime $datetime
     * @param int      $offset
     *
     * @return DateTime $datetime
     */
    public function toUTC($datetime, $offset = null)
    {
	    $offset = ($offset === null) ? $this->currentDateTimeOffset() : $offset;
        $dateInterval = DateInterval::createFromDateString(sprintf("%d minutes", $offset));
        $datetime->sub($dateInterval);

        return $datetime;
    }

    /**
     * Get string width datetime in user locale
     *
     * @param DateTime $datetime
     * @param int      $offset
     *
     * @return DateTime $datetime
     */
    public function fromUTC($datetime, $offset = null)
    {
	    $offset = ($offset === null) ? $this->currentDateTimeOffset() : $offset;
        $dateInterval = DateInterval::createFromDateString(sprintf("%d minutes", $offset));
        $datetime->add($dateInterval);

        return $datetime;
    }

} 