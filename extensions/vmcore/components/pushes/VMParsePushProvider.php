<?php
/**
 * @class VMParsePushProvider
 * VMParsePushProvider class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMParsePushProvider extends VMBasePushProvider {
	public $channel;

	/**
	 * @param array $config
	 */
	public function __construct($config = array(), $className = __CLASS__)
	{
		parent::__construct($config, $className);
	}

	/**
	 * Send push notification for devices
	 *
	 * @param string $message Message for sending
	 *
	 * @return boolean true if successful or false otherwise
	 */
	public function sendPush($message)
	{
		if(($message === null) || ($this->channel === null)) {
			$this->setResponse(self::STATUS_BAD_PARAM, Yii::t('vmcore.pushes', 'You must specify a message and a channel for sending'));
			return false;
		}

		$data = CJSON::encode(array(
			'channel' => preg_replace('/\s/', '', $this->channel),
			'data'    => array(
				'alert' => $message,
			)
		));

		if($curl = curl_init()) {
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://api.parse.com/1/push',
				CURLOPT_HTTPHEADER => array(
					'Content-Type: application/json',
					'X-Parse-Application-Id: '.$this->applicationKey,
					'X-Parse-REST-API-Key: '.$this->apiKey,
				),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $data,
			));

			$this->response = CJSON::decode(curl_exec($curl));

			curl_close($curl);

			if($this->response['result'] == true) {
				$this->setResponse(self::STATUS_OK, Yii::t('vmcore.pushes', 'Notification has been successfully sent'));
				return true;
			} else {
				$this->setResponse($this->response['code'], $this->response['error']);
			};
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function hasError() {
		return $this->code === VMPushProvider::STATUS_OK;
	}

	/**
	 * Set channel for sending push notification
	 *
	 * @param string $channel
	 */
	public function setChannel($channel)
	{
		$this->channel = $channel;
	}

}