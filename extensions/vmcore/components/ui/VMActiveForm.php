<?php
Yii::import('bootstrap.widgets.TbActiveForm');

class VMActiveForm extends TbActiveForm {
	public $options = array();
	public $htmlOptions = array();

	public function datetimePickerRow($model, $attribute, $options = array(), $htmlOptions = array()) {
		Yii::import('yiivm.extensions.datetime-picker.*');
		echo CHtml::tag('div', array('class' => 'control-group'), false, false);
		echo CHtml::tag('label', array('class' => 'control-label', 'for' => get_class($model) . '_' . $attribute), false, false);

		echo $model->getAttributeLabel($attribute);
		echo CHtml::closeTag('label');

		echo CHtml::tag('div', array('class' => 'controls'), false, false);

		$mode = (isset($options['mode']) && (in_array($options['mode'], array('time', 'date', 'datetime')))) ?
			$options['mode'] : 'datetime';

		$this->widget('CJuiDateTimePicker', array(
			'model'       => $model,
			'attribute'   => $attribute,
			'mode'        => $mode, //use "time","date" or "datetime" (default)
			'language'    => '',
			'htmlOptions' => $htmlOptions,
			'options'     => $options // jquery plugin options
		));

		echo CHtml::closeTag('div');
		echo CHtml::closeTag('div');
	}

	/**
	 * @param       $model
	 * @param       $attribute
	 * @param       $dependedAttribute
	 * @param       $dependedDataUrl
	 * @param array $data
	 * @param array $htmlOptions
	 *
	 * @return string
	 */
	public function dependentDropDownListRow($model, $attribute, $dependedAttribute, $dependedDataUrl, $data = array(), $htmlOptions = array()) {
		return $this->dropdownListRow($model, $attribute, $data, array_merge($htmlOptions, array(
			'ajax' => array(
				'type'   => 'POST',
				'url'    => CHtml::normalizeUrl($dependedDataUrl),
				'update' => '#' . CHtml::activeId($model, $dependedAttribute)
			)
		)));
	}

	public function imageUploadRow($model, $attribute, $options = array()) {
		$this->widget('VMImageUploadWidget', array(
			'model'          => $model,
			'attribute'      => $attribute,
			'width'          => $options['width'],
			'height'         => $options['height'],
			'uploadImageUrl' => $options['uploadImageUrl'],
			'removeImageUrl' => $options['removeImageUrl'],
		));
	}
}