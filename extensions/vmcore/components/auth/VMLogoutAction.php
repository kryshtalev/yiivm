<?php

class VMLogoutAction extends CAction
{
	public function run()
	{
		Yii::app()->user->logout();
	}
}