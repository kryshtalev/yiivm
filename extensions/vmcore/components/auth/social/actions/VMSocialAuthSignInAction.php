<?php
/**
 * @class VMSocialAuthSignInAction
 * Description of VMSocialAuthSignInAction class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 *
 * @deprecated {@see VMSocialConnectAction}
 */

class VMSocialAuthSignInAction extends CAction {
	public $userClass;
	public $returnUrl;
	public $componentName;
	public $identityField;
	public $loginDuration;

	public function run() {
		if (!$this->userClass) {
			throw new CException(Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'userClass')));
		}

		if(!$this->componentName) {
			throw new CException(Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'componentName')));
		}

		if(!$this->returnUrl) {
			$this->returnUrl = Yii::app()->user->returnUrl;
		}

		$service = Yii::app()->request->getQuery('service');

		if (isset($service)) {
			/**
			 * @var VMSocialAuth $component
			 */
			$component = Yii::app()->getComponent($this->componentName);
			$authIdentity = $component->getIdentity($service);
			$authIdentity->redirectUrl = $this->returnUrl;
			$authIdentity->cancelUrl   = Yii::app()->user->loginUrl;

			if ($authIdentity->authenticate()) {
				$identity = new VMSocialIdentity($this->userClass, $authIdentity, $this->identityField);

				if ($identity->authenticate()) {
					Yii::app()->user->login($identity, $this->loginDuration);
					$authIdentity->redirect();
				} else {
					$authIdentity->cancel();
				}

			}

			$this->redirect(Yii::app()->user->loginUrl);
		}
	}
} 