<?php
/**
 * @class VMAmount
 * Description of VMAmount class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMAmount extends CComponent {
	/**
	 * Amount value
	 *
	 * @var double $amount
	 */
	public $amount;

	/**
	 * Amount currency.
	 *
	 * @example USD
	 *
	 * @var string $currency
	 */
	public $currency;

	public function __construct($amount, $currency) {
		$this->amount = $amount;
		$this->currency = $currency;
	}
} 