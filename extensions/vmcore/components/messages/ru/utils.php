<?php

return array(
    'The object must be an object of {className} or its descendant' => 'Объект должен быть объектом {className} или наследоваться от него',
    'Invalid constant' => 'Неверная константа',
);