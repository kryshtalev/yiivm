<?php

return array(
	'Email' => 'E-Mail',
	'Password' => 'Пароль',
	'Remember Me' => 'Запомнить меня',
	'Incorrect email or password' => 'Неверный e-mail или пароль',
    'Bad request. Service not setting up' => 'Неверный запрос. Сервис не настроен',
    'Incorrect email or password or permission denied' => 'Неверный e-mail или пароль или Вам запрещен доступ',
);