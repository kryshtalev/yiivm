<?php

/**
 * Description of JsonServiceController
 * @author Alex
 * @property Token $debugToken
 */
class VMJsonServiceController extends CController
{
    /**
     * @var bool
     */
    public $documentationMode = false;
    /**
     * @var int
     */
    protected $code = VMServiceResponseCode::NO_ERROR;
    /**
     * @var VMSafeObject
     */
    protected $request = null;
    /**
     * @var array
     */
    protected $data = [];
    /**
     * @var array
     */
    protected $exceptions = [];

    /**
     * @param string $id
     * @param null   $module
     */
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->code       = VMServiceResponseCode::NO_ERROR;
        $this->exceptions = [];
    }

    /**
     * @return null
     */
    public function getRequest()
    {
        return $this->request;
    }

    public function init()
    {
        if ($this->documentationMode) {
            return;
        }

        if (Yii::app()->request->isPostRequest) {
            $json = CJSON::decode(file_get_contents("php://input"), false);
            if (isset($json->request)) {
                $this->request = new VMSafeObject($json->request);
            } else {
                $this->respondWithError(Yii::t('vmcore.json-service', 'There is no request node'), VMServiceResponseCode::SERVICE_ERROR);
            }
        } else {
            $this->request = new VMSafeObject(VMObjectUtils::fromArray($_GET));
        }
    }

    /**
     * @param      $mixed
     * @param int  $code
     * @param bool $return
     *
     * @return mixed
     */
    public function respondWithError($mixed, $code = VMServiceResponseCode::SERVICE_ERROR, $return = false)
    {
        $result = VMJsonServiceResponseBuilder::respondWithError($mixed, $code, $return);

        if ($return) {
            return $result;
        }

        $this->afterRespond();
    }

    /**
     * @param null $data
     * @param bool $return
     *
     * @return mixed
     */
    public function respond($data = null, $return = false)
    {
        $result = VMJsonServiceResponseBuilder::respond($data, $return);
        if ($return) {
            return $result;
        }

        $this->afterRespond();
    }

    public function afterRespond()
    {
        Yii::app()->end();
    }

    /**
     * @param array $params
     *
     * @throws VMDocumentationException
     */
    public function checkInputParameters($params = [])
    {
        if ($this->documentationMode) {
            $exception             = new VMDocumentationException();
            $exception->parameters = VMObjectUtils::fromArray($params);
            throw $exception;
        }

        $this->checkObjectParameter($params, $this->request->node);
    }

    /**
     * @param $params
     * @param $parent
     */
    private function checkObjectParameter($params, $parent)
    {
        foreach ($params as $key => $value) {
            $isArray    = is_array($value) && in_array('array', $value, true);
            $isOptional = is_array($value) && in_array('optional', $value, true);

            if ($isArray || $isOptional) {
                if (!isset($value['value'])) {
                    throw new CException(sprintf('Not found "value" for parameter "%s"', $key));
                }

                $value = $value['value'];
            }

            if (!$isOptional && $parent && !is_string($parent) && !array_key_exists($key, $parent) && !property_exists($parent, $key)) {
                $this->respondWithError(Yii::t('vmcore.json-service', 'Missed input parameter -> {key}', ['{key}' => $key]));
            }

            if ($isArray) {
                $array = null;

                if (is_array($parent) && array_key_exists($key, $parent)) {
                    $array = $parent[$key] === null ? [] : $parent[$key];
                } else if (is_object($parent) && property_exists($parent, $key)) {
                    $array = $parent->{$key} === null ? [] : $parent->{$key};
                }

                if (!$array && $isOptional) {
                    continue;
                }

                if (!is_array($array)) {
                    $this->respondWithError(Yii::t('vmcore.json-service', 'Expected array in input parameter -> {key}', ['{key}' => $key]));
                }

                foreach ($array as $item) {
                    if (!is_scalar($item)) {
                        $this->checkObjectParameter($value, $item);
                    }
                }
            } else if (is_array($value) && isset($parent->{$key})) {
                $this->checkObjectParameter($value, $parent->{$key});
            }
        }
    }

    public function run($actionId)
    {
        try {
            if (isset($this->request->locale)) {
                Yii::app()->setLanguage($this->request->locale);
            }

            parent::run($actionId);
        } catch (Exception $exception) {
            Yii::log(sprintf($exception->getMessage() . PHP_EOL . $exception->getTraceAsString()), CLogger::LEVEL_ERROR, 'api');
            $this->respondWithError($exception->getMessage());
        }
    }

    public function filterAccessControl($filterChain)
    {
        $filter = new VMJsonServiceAccessControlFilter;
        $filter->setRules($this->accessRules());
        $filter->filter($filterChain);
    }
}